﻿
using EmployeeManagementAPI.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Contracts
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employe> GetEmployees(Guid companyId, bool trackChanges); 
        Task<Employe> GetEmployee(Guid companyId, Guid employeeId, bool trackChanges);
        void CreateEmployeeForCompany(Guid companyId, Employe employee);
        void DeleteEmployee(Employe employee);
    }
}
