﻿
using EmployeeManagementAPI.Contracts;
using EmployeeManagementAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Repository
{
    public class RepositoryManager : IRepositoryManager
    {
        private RepositoryContext _repositoryContext;
        private IEmployeeRepository _employeeRepository;
        private ICompanyRepository _companyRepository;
        public RepositoryManager(RepositoryContext repository)
        {
            _repositoryContext = repository;
        }
        public ICompanyRepository Company
        {
            get
            {
                if(_companyRepository == null)
                {
                    _companyRepository = new CompanyRepository(_repositoryContext);
                }
                return _companyRepository;
            }
            
        }

        public IEmployeeRepository Employee
        {
            get
            {
                if(_employeeRepository == null)
                {
                    _employeeRepository = new EmployeeRepository(_repositoryContext);
                }
                return _employeeRepository;
            }
        }

        public void Save() => _repositoryContext.SaveChanges();

        public Task SaveAsync() => _repositoryContext.SaveChangesAsync();
    }
}
