﻿
using EmployeeManagementAPI.Contracts;
using EmployeeManagementAPI.Entities;
using EmployeeManagementAPI.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Repository
{
    public class EmployeeRepository : RepositoryBase<Employe>, IEmployeeRepository
    {
        public EmployeeRepository(RepositoryContext repositoryContext) :base(repositoryContext)
        {

        }

        public IEnumerable<Employe> GetEmployees(Guid companyId, bool trackChanges) => FindByCondition(e => 
            e.CompanyId.Equals(companyId), trackChanges).OrderBy(o => o.Name);

        public async Task<Employe> GetEmployee(Guid companyId, Guid employeeId, bool trackChanges) => await FindByCondition(e =>
            e.CompanyId.Equals(companyId) && e.Id.Equals(employeeId), trackChanges).FirstOrDefaultAsync();
        
        public void CreateEmployeeForCompany(Guid companyId, Employe employee)
        {
            employee.CompanyId = companyId;
            Create(employee);
        }

        public void DeleteEmployee(Employe employee) => Delete(employee);
    }
}
