﻿
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Formatters;
using EmployeeManagementAPI.Entities.DataTransferObject;

namespace EmployeeManagementAPI.EmployeeManager
{
    public class CsvOutputFormatter : TextOutputFormatter
    {
        /*public CsvOutputFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/csv"));
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
        }*/

        protected override bool CanWriteType(Type type)
        {
            if(typeof(CompanyDTO).IsAssignableFrom(type) || typeof(IEnumerable<CompanyDTO>).IsAssignableFrom(type))
            {
                return base.CanWriteType(type);
            }
            return false;
        }
        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var response = context.HttpContext.Response;
            var buffer = new StringBuilder();
            if(context.Object is IEnumerable<CompanyDTO>)
            {
                foreach (var company in (IEnumerable<CompanyDTO>)context.Object)
                {
                    FormatCsv(buffer, company);
                }
            }
        }

        private void FormatCsv(StringBuilder buffer, CompanyDTO company)
        {
            throw new NotImplementedException();
        }
    }
}
