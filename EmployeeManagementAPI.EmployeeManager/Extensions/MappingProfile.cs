﻿using AutoMapper;
using EmployeeManagementAPI.Entities.DataTransferObject;
using EmployeeManagementAPI.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.EmployeeManager.Extensions
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Company, CompanyDTO>()
                .ForMember(c => c.FullAddress, opt => opt.MapFrom(s => string.Join(' ', s.Address, s.Country)));
            CreateMap<Employe, EmployeesDTO>();

            //input DTO
            CreateMap<CreateCompanyDTO, Company>();
            CreateMap<CreateEmployeeDTO, Employe>();
            CreateMap<EmployeeUpdateDTO, Employe>().ReverseMap();
            CreateMap<CompanyUpdateDTO, Company>();
        }
    }
}
