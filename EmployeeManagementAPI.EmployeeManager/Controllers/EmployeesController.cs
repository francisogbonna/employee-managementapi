﻿using AutoMapper;
using EmployeeManagementAPI.Contracts;
using EmployeeManagementAPI.EmployeeManager.ActionFilters;
using EmployeeManagementAPI.Entities.DataTransferObject;
using EmployeeManagementAPI.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.EmployeeManager.Controllers
{
    [Route("api/companies/{companyId}/employees")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public EmployeesController(IRepositoryManager repo, ILoggerManager log, IMapper map)
        {
            _repository = repo;
            _logger = log;
            _mapper = map;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllForCompany(Guid companyId)
        {
            var company = await _repository.Company.GetCompanyAsync(companyId, trackChanges: false);
            if (company == null)
            {
                _logger.LogInfo($"Company with the ID: {companyId} not Found");
                return NotFound("No company");
            }
            var employees = _repository.Employee.GetEmployees(companyId, trackChanges: false);
            var employeesFound = _mapper.Map<IEnumerable<EmployeesDTO>>(employees);
            return Ok(employeesFound);
        }

        [HttpGet("{id}", Name = "GetEmployeeForCompany")]
        public async Task<IActionResult> GetEmployee(Guid companyId, Guid id)
        {
            var company = await _repository.Company.GetCompanyAsync(companyId, trackChanges: false);
            if (company == null)
            {
                _logger.LogInfo($"Company with the ID: {companyId} not Found");
                return NotFound("No company");
            }
            var employee = _repository.Employee.GetEmployee(companyId, id, trackChanges: false);
            if (employee == null)
            {
                _logger.LogInfo($"Employee with the ID: {id} not found.");
                return NotFound();
            }
            var employeeFound = _mapper.Map<EmployeesDTO>(employee);
            return Ok(employeeFound);

        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployeeForCompany(Guid companyId, [FromBody] CreateEmployeeDTO employee)
        {
            if (employee == null)
            {
                _logger.LogInfo($"Employe details is incorrect");
                return BadRequest("Employee personal details are invalid");
            }
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Invalid Model State for employee object");
                return UnprocessableEntity(ModelState);
            }

            var company = await _repository.Company.GetCompanyAsync(companyId, trackChanges: false);
            if (company == null)
            {
                _logger.LogInfo($"Company with the ID: {companyId} not found");
                return NotFound();
            }

            var employeeDetails = _mapper.Map<Employe>(employee);
            _repository.Employee.CreateEmployeeForCompany(companyId, employeeDetails);
           await _repository.SaveAsync();

            var employeeToReturn = _mapper.Map<EmployeesDTO>(employeeDetails);
            return CreatedAtRoute("GetEmployeeForCompany", new { companyId, id = employeeToReturn.Id }, employeeToReturn);
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(ValidateEmployeeExistAttribute))]
        public async Task< IActionResult> DeleteEmployeeForCompany(Guid companyId, Guid id)
        {

            var employeeForCompany = HttpContext.Items["employee"] as Employe;
            _repository.Employee.DeleteEmployee(employeeForCompany);
           await  _repository.SaveAsync();
            return NoContent();
        }

        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidateEmployeeExistAttribute))]
        public async Task< IActionResult> UpdateEmployeeForCompany(Guid companyId, Guid id, [FromBody] EmployeeUpdateDTO employee)
        {
            var employeeToUpdate = HttpContext.Items["employee"] as Employe;
            _mapper.Map(employee, employeeToUpdate);
            await _repository.SaveAsync();

            return NoContent();
        }

        [HttpPatch("{id}")]
        [ServiceFilter(typeof(ValidateEmployeeExistAttribute))]
        public async Task<IActionResult> PartialEmployeeUpdate(Guid companyId, Guid id, [FromBody] JsonPatchDocument<EmployeeUpdateDTO> patchDoc)
        {
            if (patchDoc == null)
            {
                _logger.LogError($"Invalid employee update details.");
                return BadRequest("Invalid Input received.");
            }
            var employeeEntity = HttpContext.Items["employee"] as Employe;

            var employeeToPatch = _mapper.Map<EmployeeUpdateDTO>(employeeEntity);
            patchDoc.ApplyTo(employeeToPatch, ModelState);
            TryValidateModel(employeeToPatch);
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Invalid Model State Object recieved for Employee {employeeEntity.Name}.");
                return UnprocessableEntity(ModelState);
            }
            _mapper.Map(employeeToPatch, employeeEntity);
            await _repository.SaveAsync();

            return NoContent();
        }
    }
}
