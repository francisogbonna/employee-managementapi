﻿using AutoMapper;
using EmployeeManagementAPI.Contracts;
using EmployeeManagementAPI.EmployeeManager.ActionFilters;
using EmployeeManagementAPI.EmployeeManager.ModelBinders;
using EmployeeManagementAPI.Entities.DataTransferObject;
using EmployeeManagementAPI.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employee_Management_System.Controllers
{
    [Route("api/companies")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public CompaniesController(IRepositoryManager repo, ILoggerManager log, IMapper map)
        {
            _repository = repo;
            _logger = log;
            _mapper = map;
        }

        [HttpGet]
        public async  Task<IActionResult> GetCompanies()
        {
            var companies = await _repository.Company.GetAllCompaniesAsync(trackChanges: false);
            if(companies == null)
            {
                _logger.LogInfo($"Company not Found.");
                return NotFound();
            }
            var companiesToReturn = _mapper.Map<IEnumerable<CompanyDTO>>(companies);
            return Ok(companiesToReturn);
        }

        [HttpGet("{id}", Name ="CompanyById")]
        public async Task<IActionResult> GetCompanyId(Guid id)
        {
            var company =await _repository.Company.GetCompanyAsync(id, trackChanges: false);
            if(company == null)
            {
                _logger.LogInfo($"Company with ID: {id} not Found");
                return NotFound();
            }
            var companyFound = _mapper.Map<CompanyDTO>(company);
            return Ok(companyFound);
        }

        [HttpPost]
        [ServiceFilter (typeof(ValidationFilterAttribute))]
        public IActionResult CreateCompany([FromBody]CreateCompanyDTO company)
        {
           
            var companyEntity = _mapper.Map<Company>(company);
            _repository.Company.CreateCompany(companyEntity);
            _repository.Save();

            var companyToReturn = _mapper.Map<CompanyDTO>(companyEntity);
            return CreatedAtRoute("CompanyById", new { id = companyToReturn.Id }, companyToReturn);
        }

        [HttpGet("collections/{ids}", Name ="CompanyCollection")]
        public async Task<IActionResult> GetCompanyCollection([ModelBinder(BinderType = typeof(ArrayModelBinder))]IEnumerable<Guid> ids)
        {
            if(ids == null)
            {
                _logger.LogError("Parameter Ids is null");
                return BadRequest("Parameter Ids is null");
            }
            var companyEntities = await _repository.Company.GetByIdsAsync(ids, trackChanges: false);
            if(ids.Count() != companyEntities.Count())
            {
                _logger.LogError($"Some ids are not valid in the collection");
                return NotFound();
            }
            var companiesToReturn = _mapper.Map<IEnumerable<CompanyDTO>>(companyEntities);
            return Ok(companiesToReturn);
        }

        [HttpPost("Collection")]
        public IActionResult CreateCompanyCollection([FromBody] IEnumerable<CreateCompanyDTO> companyCollection)
        {
            if(companyCollection == null)
            {
                _logger.LogError($"Company collections sent is null");
                return BadRequest(); 
            }

            var companyEntities = _mapper.Map<IEnumerable<Company>>(companyCollection);
            foreach (var company in companyEntities)
            {
                _repository.Company.CreateCompany(company);
            }
            _repository.Save();

            var companyCollectionToReturn = _mapper.Map<IEnumerable<CompanyDTO>>(companyEntities);
            var ids = string.Join(",", companyCollectionToReturn.Select(c => c.Id));

            return CreatedAtRoute("CompanyCollection", new { ids }, companyCollectionToReturn);
        }

        [HttpDelete("{id}")]
        public async  Task<IActionResult> DeleteCompany(Guid id)
        {
            var company = HttpContext.Items["company"] as Company;
            _repository.Company.DeleteCompany(company);
            await _repository.SaveAsync();

            return NoContent();
        }

        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateCompanyExistsAttribute))]
        public async Task<IActionResult> UpdateCompany(Guid id, [FromBody]CompanyUpdateDTO companyUpdate)
        {
            var companyEntity = HttpContext.Items["company"] as Company;
            _mapper.Map(companyUpdate, companyEntity);
            await _repository.SaveAsync();

            return NoContent();

        }
    }
}
