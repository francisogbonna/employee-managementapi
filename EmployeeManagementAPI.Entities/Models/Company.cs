﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Entities.Models
{
    public class Company
    {
        [Column("CompanyId")]
        public Guid Id { get; set; }

        [Required(ErrorMessage ="Company Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Company Locatio address is required")]
        public string Address { get; set; }

        public string Country { get; set; }
        public ICollection<Employe> Employees { get; set; }

    }
}
