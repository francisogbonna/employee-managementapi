﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Entities.Models
{
    public class Employe
    {
        [Column("EmployeeId")]
        public Guid Id { get; set; }

        [Required(ErrorMessage ="Employee name is a required field")]
        public string  Name { get; set; }

        [Required(ErrorMessage ="Employee Age is required")]
        public int Age { get; set; }

        [Required(ErrorMessage ="Position is required")]
        [MaxLength(25, ErrorMessage ="Maximun character of 25 exceeded.")]
        public string  Position { get; set; }

        [ForeignKey(nameof(Company))]
        public Guid CompanyId { get; set; }
        public Company Company { get; set; }
    }
}
