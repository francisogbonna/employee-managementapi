﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Entities.DataTransferObject
{
    public class CreateCompanyDTO
    {
        [Required(ErrorMessage ="Company Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Company Address is required")]
        public string Address { get; set; }

        [Required(ErrorMessage ="Company Location Country is required.")]
        public string Country { get; set; }

        //populate employees while creating a company
        public IEnumerable<CreateEmployeeDTO> Employees { get; set; }
    }
}
