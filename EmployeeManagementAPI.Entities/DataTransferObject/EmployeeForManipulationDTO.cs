﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Entities.DataTransferObject
{
    public abstract class EmployeeForManipulationDTO
    {
        [Required(ErrorMessage = "Name field is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Age is a required field.")]
        [Range(18, int.MaxValue, ErrorMessage = "Employee must be above 18years.")]
        public int Age { get; set; }

        [Required(ErrorMessage = "Employee's position is required.")]
        [MaxLength(20, ErrorMessage = "max length for position is 20 characters.")]
        public string Position { get; set; }
    }
}
