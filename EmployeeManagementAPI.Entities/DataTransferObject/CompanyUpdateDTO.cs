﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagementAPI.Entities.DataTransferObject
{
    public class CompanyUpdateDTO
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }

        public IEnumerable<CreateEmployeeDTO> Employees { get; set; }
    }
}
